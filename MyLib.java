import java.io.*;
import java.util.List;
import java.util.ArrayList;
import java.lang.Integer;


public class MyLib {

	private static byte[] getFileData(File file) throws IOException {
		List<Integer> arr = new ArrayList<Integer>();
		FileInputStream fileIn = new FileInputStream(file);
		BufferedInputStream in = new BufferedInputStream(fileIn);
		int i;
		while((i=in.read())!=-1) {
			arr.add(i);
		}
		byte[] bytes = new byte[arr.size()];
		for(int x=0; x<arr.size(); x++) {
			bytes[x] = new Integer(arr.get(x)).byteValue();
		}
		return bytes;
	}

	public static void copyDirectory(String directoryPath, String toPath) throws IOException {
		File folder = new File(directoryPath);
		File[] files = folder.listFiles();
		for(File file: files) {
			if(file.isDirectory()) {
				File x = new File(toPath+"\\"+file.getName());
				x.mkdir();
				copyDirectory(directoryPath+"\\"+file.getName(), toPath+"\\"+file.getName());
			} else {
				String fileName = file.getName();
				byte[] fileData = getFileData(file);
				File newFile = new File(toPath+"\\"+fileName);
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(newFile));
				out.write(fileData);
				out.flush();
				out.close();
			}
		}
	}

}